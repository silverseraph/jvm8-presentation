\documentclass[lualatex,mathserif,sanserif]{beamer}

\usepackage[english]{babel}
\usepackage{textcomp}
\usepackage{MnSymbol}
\usepackage{hyperref}
\usepackage{ragged2e}
\usepackage{color}

\usetheme{Warsaw}
\usecolortheme{beaver}

\setbeamertemplate{headline}{%
    \leavevmode%
    \hbox{%
        \begin{beamercolorbox}[wd=\paperwidth,ht=2.5ex,dp=1.125ex]{palette quaternary}%
            \insertsectionnavigationhorizontal{\paperwidth}{}{\hskip0pt plus1filll}
        \end{beamercolorbox}%
    }
}

\defbeamertemplate*{footline}{shadow theme}
{%
    \leavevmode%
    \hbox{\begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm plus1fil,rightskip=.3cm]{author in head/foot}%
        \usebeamerfont{author in head/foot}\insertframenumber\,/\,\inserttotalframenumber\hfill\insertshortauthor
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.5ex,dp=1.125ex,leftskip=.3cm,rightskip=.3cm plus1fil]{title in head/foot}%
        \usebeamerfont{title in head/foot}\insertshorttitle%
    \end{beamercolorbox}}%
    \vskip0pt%
}

\beamertemplatenavigationsymbolsempty

\AtBeginSection[]{
    \begin{frame}
        \vfill
        \centering
        \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
            \usebeamerfont{title}\insertsectionhead\par%
        \end{beamercolorbox}
        \vfill
    \end{frame}
}

\title[JVM8 Spec]{JVM8 Specification}
\subtitle{A Whirlwind Tour of the JVM}
\author{Andrew Benton}
\institute{Comcast/NBCUniversal}
\date{\today}
\subject{CSV Brown Bag}

\begin{document}
    \frame{\titlepage}

\section{Introduction}

    \begin{frame}
        \frametitle{Overview}
        \tableofcontents
    \end{frame}

    \begin{frame}
        \frametitle{Goals}
        \begin{itemize}
            \item Understanding of JVM's strengths\\
                \begin{itemize}
                    \item Semi-portable format \\
                    \item Easy to implement \\
                    \item Mostly simple languages \\
                \end{itemize}
            \pause
            \item Understanding of JVM's weaknesses\\
                \begin{itemize}
                    \item Built-in limits to certain method / constant / class properties \\
                        \textit{e.g.} Dalvik limits 65k methods per DEX file \\
                    \item Stack based architecture (performance) \\
                    \item Limit of 256 unique instructions \\
                    \item All classes are dynamically loaded \\
                \end{itemize}
        \end{itemize}
    \end{frame}

\section{Bytecodes and Execution}

    \begin{frame}
        \frametitle{Execution basics}
        \begin{itemize}[<+->]
            \item Call stack
            \item Heap
            \item Instructions
            \item Execution stack - context for execution on a per-function basis
            \item Registers vs Stack \\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Registers vs Stack}
        \begin{itemize}[<+->]
            \item Registers are faster \\
            \item Registers are limited in number \\
            \item Stack has fewer unique instructions \\
            \item Stack requires more instructions for a similar effect\\
            \item Registers are significantly faster\\
            \item All consumer hardware now works on registers \\
            \item Some older mainframes worked on stack\\
            \item Native stack is simpler to context switch from,
                \textit{but the JVM isn't native}
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Execution stack}
        \begin{itemize}
            \item Initially empty
            \item Instructions modify the stack with push / pop
            \item In order to operate on anything, it must be present on the stack
            \item Values on the execution stack can be saved off, \textit{e.g.} into objects
        \end{itemize}
        \begin{exampleblock}{public int add(int a, int b) \{ ... \}}
            \begin{tabular}{l|l||l}
                index & instruction & effect \\
                \hline
                0  & iload\_1 & push $a$ onto the stack \\
                1  & iload\_2 & push $b$ onto the stack \\
                2  & iadd     & pop $a$ and $b$ from the stack, add them, \\
                   &          & \quad and push the result onto the stack \\
                3  & ireturn  & exit the function and return the \\
                   &          & \quad value from the top of the stack
            \end{tabular}
        \end{exampleblock}
    \end{frame}

    \begin{frame}
        \frametitle{Example Stack Addition - \texttt{add(3,4);}}
        \begin{columns}[T]
            \column{.5\textwidth}
            \begin{center}
                \begin{tabular}{l}
                    Instructions \\
                    \hline
                    \only<1>{\color{red}{iload\_1}} \only<2->{iload\_1} \\
                    \only<2>{\color{red}{iload\_2}} \only<1,3->{iload\_2} \\
                    \only<3>{\color{red}{iadd}}     \only<1-2,4->{iadd} \\
                    \only<1-3>{ireturn}             \only<4->{\color{red}{ireturn}} \\
                \end{tabular}
            \end{center}

            \column{.5\textwidth}
            \begin{center}
                \begin{tabular}{l}
                    Stack \\
                    \hline
                    \only<1>{\color{red}{3}} \only<2>{3} \only<3>{\color{red}{7}} \\
                    \only<2>{\color{red}{4}} \\
                \end{tabular}
            \end{center}
        \end{columns}

        \begin{exampleblock}{Explanation}
            \only<1>{The value of 3 is added to the stack from argument 0}
            \only<2>{The value of 4 is added to the stack from argument 1}
            \only<3>{3 and 4 are popped from the stack by the \textit{iadd} instruction, summed, and the result is pushed onto the stack}
            \only<4>{\textit{ireturn} pops a value from the stack and returns it}
        \end{exampleblock}
    \end{frame}

    \begin{frame}
        \frametitle{Object Constructor}
        \begin{exampleblock}{class Test \{ public Test(String s) \{ ... \} \}}
            \begin{tabular}{l|l||l}
                index  & instruction       & effect \\
                \hline
                0      & aload\_0          & load the Test object that has \\
                       &                   & \quad $already$ been allocated \\
                1      & invokespecial \#1 & calls the Object:$<$init$>$() and \\
                       &                   & \quad consumes the Test object \\
                4      & aload\_0          & reload the already created Test object \\
                5      & aload\_1          & load argument $s$ onto the stack \\
                6      & putfield \#3      & put $s$ into a member field \\
                9      & return            & returns void
            \end{tabular}
        \end{exampleblock}
    \end{frame}

    \begin{frame}
        \frametitle{Object Instantiation}
        \begin{exampleblock}{Test t = new Test("hello, world!");}
            \begin{tabular}{l|l||l}
                index & instruction & effect \\
                \hline
                0  & new \#9            & Allocate Test object $t$ \\
                   &                    & \quad (not initialized) on stack\\
                3  & dup                & Duplicate the reference on \\
                   &                    & \quad the top of the stack (Test) \\
                4  & ldc \#11           & push "hello, world!" onto the stack \\
                6  & invokespecial \#12 & initialize $t$ with "hello, world!" \\
            \end{tabular}
        \end{exampleblock}
        Final state of the stack has a single reference to $t$
    \end{frame}

    \begin{frame}
        \frametitle{Viewing Your Bytecode}

        \begin{exampleblock}{Command Sequence}
            \begin{itemize}
                \item \texttt{javac -g MyClass.java}    - Compile with debug symbols\\
                \item \texttt{javap -c -l MyClass} - Output bytecode and lines\\
            \end{itemize}
        \end{exampleblock}

        \begin{figure}
            \includegraphics[width=0.65\textheight]{imgs/javap_stdout.png}
            \caption{Java source code \rightarrow Bytecode}
        \end{figure}

    \end{frame}

\section{Objects and Primitives}

    \begin{frame}
        \frametitle{Basic types}
        \begin{itemize}
            \item Basic types on JVM (and size): \\
                \begin{itemize}
                    \item object (?) \\
                    \item array (?) \\
                    \item double (8) \\
                    \item float (4) \\
                    \item short (2) \\
                    \item long (8) \\
                    \item int (4) \\
                    \item char (2) \\
                    \item byte (1) \\
                    \item boolean (1) \\
                \end{itemize}
            \item \textit{JVM has no special objects} \\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{A Quick Note...}
        \framesubtitle{On Primitives}
        \begin{itemize}[<+->]
            \item Due to the sizing of double and long, they are treated differently from other primitives, but this doesn't matter much to the user \\
            \item The primitive boxing types are \textit{very} different from their primitive types \\
                $e.g.$ A $Double$ will be 28 bytes, but a $double$ will be 8 bytes.
            \item They are nullable, but much slower, larger, and garbage collected
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Understanding Naming}

        \begin{itemize}
            \item JVM uses name mangling to store types
            \item JVM distinguishes types based on name \textit{only}
            \item JVM verifies types based on (name, ClassLoader) tuple
            \item Mangling conventions: \\
                \begin{tabular}{l|l||l|l}
                    Type & Convention & Type & Convention \\
                    object & L$<$object\_name$>$; & array & [$<$type$>$ \\
                    double & D & float & F \\
                    long & J & int & I \\
                    short & S & char & c \\
                    byte & B & boolean & Z \\
                    void & V & & \\
                \end{tabular}
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Example Name Mangling...}
        \framesubtitle{With Parameters}
        \begin{exampleblock}{Readable \rightarrow Mangled}
            \begin{tabular}{l||l|l}
                Description & Type & Mangled \\
                \hline
                Single double & double & D \\
                Single class & Double & Ljava/lang/Double; \\
                Array of double & double[] & [D \\
                2D Array of double & double[][] & [[D \\
                Array of Double & Double[] & [Ljava/lang/Double; \\
                2D Array of Double & Double[][] & [[Ljava/lang/Double; \\
            \end{tabular}
        \end{exampleblock}
    \end{frame}

    \begin{frame}[shrink]
        \frametitle{Example Name Mangling...}
        \framesubtitle{With Functions}
            \begin{itemize}
                \item Signature only: $Object\ method(int\ i, double\ d, Thread\ t)$ \\
                    \qquad \texttt{(IDLjava/lang/Thread;)Ljava/lang/Object;} \\
                \item Instance method: $void\ com.test.MyTest.a(int\ a)$ \\
                    \qquad \texttt{com/test/myTest.a:(I)V} \\
                \item Static method: $double[]\ com.test.MyTest.b(double\ d)$ \\
                    \qquad \texttt{com/test/MyTest.b:(D)[D} \\
                \item There is no naming convention to distinguish static methods
                \item Static methods are distinguished in $.class$ format and called with $invokestatic$
            \end{itemize}
    \end{frame}

\section{VM Startup}

    \begin{frame}
        \frametitle{In the Beginning...}
        \begin{itemize}
            \item The JVM comes with a built-in bootstrapping ClassLoader \\
            \item Bootstrapper finds $public\ static\ void\ main(String[]\ args)$ \\
            \item This class will be loaded first \\
            \item In order for this class to be loaded, any dependencies must also be loaded \\
            \item After all dependencies for $this$ class are loaded and verified, the execution proceeds
            \item I won't go into verification.  That is about 200 pages of Prolog in the JVM Spec.
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{ClassLoaders}
        \begin{itemize}
            \item The original ClassLoader is the bootstrapper
            \item ClassLoaders can:
                \begin{itemize}
                    \item Choose to either load a class \\
                    \item Delegate to another class loader \\
                \end{itemize}
            \item When loading a class, the ClassLoader passes a byte array containing $.class$ data.
            \item If a ClassLoader fails to load a class, then all dependant classes also fail.
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Linking}
        \begin{itemize}
            \item Given that the loading has succeeded, linking will proceed \\
            \item Verification of the $.class$ format runs first.  If this fails, then linking fails. \\
            \item Preparation will set static fields to default values, but not initialize \\
            \item Linking finishes with resolution \\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Resolution}
        \begin{itemize}
            \item All symbolic references must be resolved for resolution to pass \\
            \item If a new class is referenced, then it too must be loaded and resolved \\
            \item If a class is referenced, all superclasses and superinterfaces must also be resolved \\
            \item Access permissions to the symbolic references are also checked \\
                \quad $Aside:$ This can be worked around by using run-time resolution and overriding the security context.  Please don't do this.
            \item When all symbols in all referenced classes are resolved, resolution is successful \\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Initialization}
        \begin{itemize}
            \item Once a class calls $getstatic$, $putstatic$, or $invokestatic$ for the first time, initialization is required \\
            \item JVM jakes a best effort to initialize all classes.  It seems like most common JVMs don't strictly follow this rule, as you can have circular initialization without error.
            \item By the spec, when a class is initializing, it becomes locked and remains locked until it finishes the initialization. \\
            \item This proceeds recursively from the instruction that began the initialization \\
        \end{itemize}
    \end{frame}

\section{Garbage Collection}

    \begin{frame}
        \frametitle{GC Basics in 30 Seconds}

        \begin{itemize}[<+->]
            \item Track objects in use, free memory when not used\\
            \item JVM doesn't specify a method\\
            \item Popular methods are:
                \begin{itemize}
                    \item Reference Counting\\
                    \item Mark and Sweep\\
                    \item Concurrent Mark and Sweep\\
                    \item Generational\\
                \end{itemize}
            \item Essentially anything that's not manual management\\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{GC in Java}
        \begin{itemize}[<+->]
            \item OracleJDK and OpenJDK use Generational GC by default\\
            \item Use of GC \textit{can} improve performance\\
                \begin{itemize}
                    \item Avoid fragmentation\\
                    \item Improve cache hit rate\\
                    \item Group malloc and free calls\\
                \end{itemize}
            \item Most often it doesn't, even for Generational GC\\
                \begin{itemize}
                    \item ~0\% hit at 4x footprint\\
                    \item 17\% hit at 3x footprint\\
                    \item 70\% hit at 2x footprint\\
                \end{itemize}
            \item Can lead to dropped frames and \sim30-40ms GC pauses\\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Types of GC in Java}
        \begin{itemize}
            \item Serial - \\
                \begin{itemize}[<+->]
                    \item Lowest CPU overhead \\
                    \item Fully Stop-The-World \\
                    \item Traditional Mark and Sweep \\
                \end{itemize}
            \item Parallel - \\
                \begin{itemize}[<+->]
                    \item Higher CPU overhead with multiple threads \\
                    \item Fully Stop-The-World \\
                    \item Default for OracleJVM and OpenJVM \\
                \end{itemize}
        \end{itemize}
        \begin{figure}
            \includegraphics[width=0.75\textwidth]{imgs/Java-GC-mark-and-sweep.png}
            \caption{Mark and Sweep traversal via \href{https://plumbr.eu/handbook/garbage-collection-algorithms}{plumbr.eu}}
        \end{figure}
    \end{frame}

    \begin{frame}
        \frametitle{Types of GC in Java (Con't)}
        \begin{itemize}
            \item Concurrent Mark and Sweep (CMS) - \\
                \begin{itemize}[<+->]
                    \item Uses threads = $\frac{1}{4}$CPUs \\
                    \item Sometimes Stop-The-World \\
                    \item Compaction requires Stop-The-World \\
                    \item Tries to run GC concurrently with program \\
                \end{itemize}
            \item G1 (Garbage First) - \\
                \begin{itemize}[<+->]
                    \item Keeps generations for different regions of memory \\
                    \item Sweeps regions at different frequencies based on generation \\
                    \item Can \textit{still} be Stop-The-World \\
                \end{itemize}
        \end{itemize}

        \begin{figure}
            \includegraphics[width=0.75\textwidth]{imgs/g1-shading.png}
            \caption{G1 Region Shading via \href{https://plumbr.eu/blog/garbage-collection/g1-garbage-collector-in-action}{plumbr.eu}}
        \end{figure}
    \end{frame}

    \begin{frame}
        \frametitle{GC Performance Falloff}

        \begin{figure}
            \includegraphics[width=0.8\textwidth]{imgs/gc_perf.png}
            \caption{Relative Memory Footprint vs Performance - \href{http://www-cs.canisius.edu/~hertzm/gcmalloc-oopsla-2005.pdf}{Hertz 2005}}
        \end{figure}
    \end{frame}

\section{JVM Future}

    \begin{frame}
        \frametitle{OpenJVM 9 Improvements}

        \begin{itemize}[<+->]
            \item Compact Strings\\
            \item Unified Logging\\
            \item PubSub Framework\\
            \item Dynamically invocable properties\\
            \item Switch default to G1 GC\\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Beyond JVM 9...}

        \begin{itemize}
            \item Value types \pause - Due to GC limitations and memory sizes\\
                \pause
            \item Atomics \pause - Because synchronization is bad for performance and caches\\
                \pause
            \item Specialized Generics (Refried Bytecode) \pause - memory again\\
                \pause
            \item Maybe something like templating?\\
        \end{itemize}
    \end{frame}

\section{Conclusions}

    \begin{frame}
        \frametitle{Wrapping up...}
        \begin{itemize}[<+->]
            \item The JVM is well established and does a decent job \\
            \item Fantastic tooling support \\
            \item JVM \textit{theoretically} runs bytecode, but most often is JIT'd \\
            \item Whenever objects can be avoided reasonably, do so \\
            \item Must be aware of the GC, its benefits, and its limitations \\
            \item Future JVMs looking to regain some benefits of native code \\
            \item JVM will likely need to increase method numbers and change instruction option size \\
            \item Better alternative to scripting languages for performance and safety \\
        \end{itemize}
    \end{frame}

    \begin{frame}
        \begin{center}
            \Huge Questions?
        \end{center}
    \end{frame}

\end{document}
